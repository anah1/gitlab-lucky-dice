
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    "use strict";
    const gREQUEST_STATUS_OK = 200;
    const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;
    const gBASE_URL = "http://42.115.221.44:8080/devcamp-lucky-dice/";
    const gUTF8_TEXT_APPLICATION_HEADER = "application/json;charset=UTF-8";
    
    

    /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
    function getDataUser(parmamObjectUser){
        "use strict";
        var vGetElementUsername = document.getElementById("inp-user-name");
        var vGetElementfirstname = document.getElementById("inp-first-name");
        var vGetLastName = document.getElementById("inp-last-name");
        parmamObjectUser.username = vGetElementUsername.value.trim();
        parmamObjectUser.firstname = vGetElementfirstname.value.trim();
        parmamObjectUser.lastname = vGetLastName.value.trim();
        

    }
    
    function isValid(parmamObjectUser){
        "use strict";
        if(parmamObjectUser.username==""){
            alert("Vui lòng nhập Username");
            
            return false;
        }
        if(parmamObjectUser.firstname==""){
            alert("Vui lòng nhập first Name");
            return false;
        }
        if(parmamObjectUser.lastname==""){
            alert("Vui lòng nhập last Name");
            return false;
        }
        return true;
    }

    function callApiService(paramXmlHttpDice,paramObjectUser,paramEndpoint){
        "use strict";
        //Gọi API cho task 23B.50
        if(paramEndpoint=="dice"){
            paramXmlHttpDice.open("POST",gBASE_URL + paramEndpoint,true);
            paramXmlHttpDice.setRequestHeader("Content-Type",gUTF8_TEXT_APPLICATION_HEADER);
            paramXmlHttpDice.send(JSON.stringify(paramObjectUser));//chuyển object sang string
        }
          //Gọi API cho task 23B.60
        if(paramEndpoint=="dice-history?username="){
            paramXmlHttpDice.open("GET",gBASE_URL + paramEndpoint + paramObjectUser.username,true);
            paramXmlHttpDice.send();
        }
     /*   if(paramEndpoint=="/voucher-history?username="){
            paramXmlHttpDice.open("GET",gBASE_URL + paramEndpoint + paramObjectUser.username,true);
            paramXmlHttpDice.send();
        }*/

       
    }

    function processReponse(paramXmlHttpDice){
        "use strict";
        console.log(paramXmlHttpDice.responseText);
        console.log(JSON.parse(paramXmlHttpDice.responseText));
        var vConvertToObject = JSON.parse(paramXmlHttpDice.responseText);
        // gọi hàm hiển thị dices
        var vDiceReady = vConvertToObject.dice;
        console.log("dice result: " +vDiceReady);
        changeDice(vDiceReady);
        changeNotification(vDiceReady);
        // gọi hàm hiển thị voucher
        changeVoucherID(vConvertToObject);
        changeGift(vConvertToObject);
    }
    
  // Task 23B.60
    function processReponseHistoryDices(paramXmlHttpDicesHistory){
        "use strict"
        var vStringJsonReponse = paramXmlHttpDicesHistory.responseText;
        console.log(vStringJsonReponse);
        var vConvertToObjectHistoryDices = JSON.parse(vStringJsonReponse);
        console.log(vConvertToObjectHistoryDices.dices);
       
        deleteDataTable();
        displayHistoryDices(vConvertToObjectHistoryDices);
    }

    function  deleteDataTable(){
        var vGetElementTable = document.getElementById("history-placeholder-table");
        while(vGetElementTable.rows.length>1){
            vGetElementTable.deleteRow(1);
        }
        vGetElementTable.deleteRow(0);
    }

    function displayHistoryDices(paramArrDices){
        var vGetElementTable = document.getElementById("history-placeholder-table");
        // thêm phần Header
        var vNewRowHeader = vGetElementTable.insertRow(-1);
        var vNewCellLuot = vNewRowHeader.insertCell(0);
        var vNewCellDice = vNewRowHeader.insertCell(1);
        vNewCellLuot.style.fontWeight = "bold";
        vNewCellDice.style.fontWeight= "bold";
        vNewCellLuot.innerHTML = "Lượt";
        vNewCellDice.innerHTML ="Dices";
        for(let bI = 0; bI < paramArrDices.dices.length; bI++){
            var bNewRowData = vGetElementTable.insertRow(-1);
            var bNewCellNumber = bNewRowData.insertCell(0);
            var bNewCellResultDices = bNewRowData.insertCell(1);
            bNewCellNumber.innerHTML = bI + 1;
            bNewCellResultDices.innerHTML = paramArrDices.dices[bI]

        }
    }


    function changeVoucherID(paramObjectUser){
        "use strict";
        var vPvoucherID = document.getElementById("p-voucher-id");
        var vPvoucherPercent = document.getElementById("p-voucher-percent");
        if(paramObjectUser.voucher!=null){
            vPvoucherID.innerHTML = paramObjectUser.voucher.maVoucher;
            vPvoucherPercent.innerHTML = paramObjectUser.voucher.phanTramGiamGia + "%";
        }
        else{
            vPvoucherID.innerHTML ="";
            vPvoucherPercent.innerHTML="";

        }
    }

    function changeGift(paramObjectUser){
        "use strict";
        var vImgPresent = document.getElementById("img-present");
        var vTenPhanthuong = paramObjectUser.prize;
        switch(vTenPhanthuong){
            case "Mũ":
            vImgPresent.src = "LuckyDiceImages/hat.jpg";
                break;
            case "Áo":
            vImgPresent.src ="LuckyDiceImages/t-shirt.jpg";
                break;
            case "Xe máy":
            vImgPresent.src ="LuckyDiceImages/xe-may.jpg";
                break;
            case "Ô tô":
            vImgPresent.src ="LuckyDiceImages/car.jpg";
                break;
            default:
            vImgPresent.src ="LuckyDiceImages/no-present.jpg";            
        }

    }

    function changeDice(paramDiceReady){
        "use strict";
        var vImgDice = document.getElementById("img-dice");
        vImgDice.src = "LuckyDiceImages/" + paramDiceReady +  ".png";
    }

    function changeNotification(paramDiceReady){
        "use strict";
        var vNotificaton = document.getElementById("p-notification-dice");
        if(paramDiceReady >=4){
            vNotificaton.innerHTML = "Chúc mừng bạn hãy chơi tiếp lần nữa";
        }
        else{
            vNotificaton.innerHTML = "Chúc mừng bạn may mắn lần sau";
        }
    }
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

    function onBtnNemClick(){
        "use strict";
        var vObjectUser = {
            username: "",
            firstname:"",
            lastname:"",
        }
        // bước 1 lấy thông tin
        getDataUser(vObjectUser);
        //bước 2 kiểm tra thông tin 
        var vIsvalid = isValid(vObjectUser);
        //bước 3 gọi API
        if(vIsvalid){
            var vXmlHttpDice = new XMLHttpRequest();
            var vEndpoint = "dice"; 
            callApiService(vXmlHttpDice,vObjectUser,vEndpoint);
            vXmlHttpDice.onreadystatechange = function(){
                if(this.readyState==gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK){
                    // Bước 4 xử lý phản hồi
                    processReponse(vXmlHttpDice);
                }
            }
        }


    }


    // task 23B60
    function onBtnDicesHistoryClick(){
        "use strict";
        console.log("Nút Dice History được ấn");
        var vObjectUser ={
            username: "",
            firstname:"",
            lastname:"",
        }
        getDataUser(vObjectUser);
        var vIsvalid = isValid(vObjectUser);
        if(vIsvalid){
            var vXmlHttpDicesHistory = new XMLHttpRequest();
            // call api
            var vEndpointHistoryDice = "dice-history?username=";
            callApiService(vXmlHttpDicesHistory, vObjectUser, vEndpointHistoryDice);
            vXmlHttpDicesHistory.onreadystatechange = function(){
                if(this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status== gREQUEST_STATUS_OK){
                    processReponseHistoryDices(vXmlHttpDicesHistory);
                }
            }
        }

    }
    

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/


